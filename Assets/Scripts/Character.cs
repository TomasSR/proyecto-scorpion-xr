﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.SocialPlatforms;
using UnityEngine.UIElements;

public class Character : MonoBehaviour
{
    public CharacterController MainCharacter;

    private float left = 0;
    private float rigth = -180;

    [Header("Movment and Gravity options")]
    [Range(0f, 15f)]
    public float playerSpeed = 7f;
    [Range(0f, 30f)]
    public float gravity = 20f;


    [Header("Jump options")]
    public bool jumpOnOff = false;
    [Range(0, 40)]
    public int jump = 20;

    Animator anim;


    Vector3 characterDir = Vector3.zero;


    void Start()
    {
        MainCharacter = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }


    void Update()
    {
        walking();
    }

    //Caminando
    void walking()
    {
        if (MainCharacter.isGrounded)
        {
            //Movimiento del personaje
            characterDir = new Vector3(Input.GetAxis("Horizontal"), 0, 0);
            characterDir *= playerSpeed;

            //Animacion del personaje
            if (MainCharacter.velocity.magnitude > -1f)
            {
                anim.SetFloat("velocity", MainCharacter.velocity.magnitude);
            }
            else
            {
                anim.SetFloat("IdleSynty", MainCharacter.velocity.magnitude);
            }

            //Salto
            if (jumpOnOff == true)
            {
                if (Input.GetButton("Jump"))
                {
                    characterDir.y = jump;
                }
            }
        }
        else
        {
            characterDir.x = Input.GetAxis("Horizontal") * playerSpeed;
        }

        // Direccion del cuerpo al caminar
        if (Input.GetAxis("Horizontal") != 0)
        {

            if (Input.GetAxis("Horizontal") < 0)
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, rigth, transform.rotation.z);
                
            }
            else
            {
                transform.rotation = Quaternion.Euler(transform.rotation.x, left, transform.rotation.z);
            }

        }

        characterDir.y -= gravity * Time.deltaTime;

        MainCharacter.Move(characterDir * Time.deltaTime);
    }
}