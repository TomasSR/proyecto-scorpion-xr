﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TreeEditor;
using UnityEditor;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{

    private bool opened = false;
    bool inTheDoor = false;
    private Animator anim;

    // Pivote
    private float gizmoSize = .75f;
    private Color gizmoColor = Color.yellow;


    void Start()
    {

    }

    void Update()
    {
        if (inTheDoor == true)
        {
            if (Input.GetKeyDown(KeyCode.C))
            {
                anim = transform.GetComponentInParent<Animator>();
                opened = !opened;
                anim.SetBool("OpenDoor", opened);
            }
        }
    }

    void OnTriggerStay(Collider Other)
    {
        inTheDoor = true;
    }

    void OnTriggerExit (Collider Other)
    {
        inTheDoor = false;
    }

    // Pivote
    void OnDrawGizmos()
    {
        Gizmos.color = gizmoColor;
        Gizmos.DrawWireSphere(transform.position, gizmoSize);
    }
}
