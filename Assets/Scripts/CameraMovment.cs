﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovment : MonoBehaviour
{
    public GameObject mainCharacterFollow;

    void Start()
    {

    }

    void Update()
    {
        transform.position = mainCharacterFollow.transform.position + new Vector3(2, 8f, -14.5f);
        transform.rotation = Quaternion.Euler(20, 0, 0);
    }
}
